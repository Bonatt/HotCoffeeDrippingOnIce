import ROOT
#import numpy as np
import random
#import time
#from collections import Counter
#import operator
import math


#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
ROOT.gStyle.SetPalette(51); # 51:blue. 52:lo=k/hi=w. 53:lo=r/hi=y, 56 rev. 54:lo=b/hi=y. 55:lo=b/hi=r, pale rainbow. none:rainbow 
# Palettes, see https://root.cern.ch/doc/master/classTColor.html#C05. Or for ROOT 5.34: https://people.nscl.msu.edu/~noji/colormap
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(1, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(1);  #0,1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();




# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()
def sqrt(x):
  return math.sqrt(x)
# Redefine pi to something shorter
pi = math.pi




### Hot coffee dripping on ice.
# As hot water drips onto the ice, it melts.
# It melts at some rate, and the shape is semi random?
# It forms streams, and tends to melt into those streams even more.
# Code something that drops drops onto ice, breaks one square of it after n drops.
# Randomly choose direction.
# Prefers direction towards lower ice. Derivative?
#...
# Like space invador thing except some kind of trend.









### Dimensions of ice cube
width = 100
height = 100
center = width/2















###### Only use histo to plot, not to generate melted ice...?
### Create canvas with ratio that scales with width,height
c = ROOT.TCanvas('c', 'c', 720*width/height, 720)
c.SetTopMargin(0)
c.SetBottomMargin(0)
c.SetLeftMargin(0)
c.SetRightMargin(0)

### Generate 2D ice cube.
### Initialize 2D hist to be filled with icecube
hc = ROOT.TH2D('hc', 'hc', width,0,width, height,0,height)
hc.SetStats(0)
hc.SetTitle(';;')

for w in range(width):
  for h in range(height):
    #h.Fill(w,h,1)
    hc.SetBinContent(w,h,0)







# Loop from 0 to height...
xpos = center
deltax = 1
melt = 0.1
for h in range(height,-1,-1): 

  #print xpos

  r = random.random()
  if r > 0.5:
    direction = +1
  else:
    direction = -1

  hc.SetBinContent(xpos+direction*deltax,h,1)



  if (r > 0.5+melt*4.5) or (r < 0.5-melt*4.5): neighbors = 5
  elif (r > 0.5+melt*4) or (r < 0.5-melt*4): neighbors = 4
  elif (r > 0.5+melt*3) or (r < 0.5-melt*3): neighbors = 3
  elif (r > 0.5+melt*2) or (r < 0.5-melt*2): neighbors = 2
  elif (r > 0.5+melt*1) or (r < 0.5-melt*1): neighbors = 1
  else: neighbors = 0
  print neighbors


  directionn = random.sample( [[-1,-1],[-1,0],[-1,1],[0,-1],[0,1],[1,-1],[1,0],[1,1]] , neighbors) 
   
  #for n in range(1,neighbors+1):
  for n in directionn:
    '''
    rn = random.random()
    if rn > 0.75: xdirection = +1
    elif rn > 0.5: xdirection = -1
    else: xdirection = 0
    if rn < 0.25: ydirection = +1
    elif rn < 0.5: ydirection = -1
    else: ydirection = 0
    '''
    #rn = random.choice([[-1,-1],[-1,0],[-1,1],[0,-1],[0,1],[1,-1],[1,0],[1,1]])
    #xdirection = rn[0]
    #ydirection = rn[1]
      
    hc.SetBinContent(xpos+direction*deltax+n[0],h+n[1],2)  
    #print '',n,xdirection,ydirection
    print '',n



  xpos = xpos+direction*deltax






hc.Draw('col')
c.SaveAs('Drip1_test3.png')
