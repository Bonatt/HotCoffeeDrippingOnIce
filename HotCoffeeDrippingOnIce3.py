import ROOT
import numpy as np
import random
import math


#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
ROOT.gStyle.SetPalette(51); # 51:blue. 52:lo=k/hi=w. 53:lo=r/hi=y, 56 rev. 54:lo=b/hi=y. 55:lo=b/hi=r, pale rainbow. none:rainbow 
# Palettes, see https://root.cern.ch/doc/master/classTColor.html#C05. Or for ROOT 5.34: https://people.nscl.msu.edu/~noji/colormap
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(1, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(1);  #0,1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();




# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()
def sqrt(x):
  return math.sqrt(x)
# Redefine pi to something shorter
pi = math.pi




### Hot coffee dripping on ice.
# As hot water drips onto the ice, it melts.
# It melts at some rate, and the shape is semi random?
# It forms streams, and tends to melt into those streams even more.
# Code something that drops drops onto ice, breaks one square of it after n drops.
# Randomly choose direction.
# Prefers direction towards lower ice. Derivative?
#...
# Like space invador thing except some kind of trend.









### Dimensions of ice cube
width = 100
height = 100
center = 50

### Make 100x100 array filled with 100
# np.array(Icecube).shape = (100, 100)
# Icecube[y][x]
Icecube = np.zeros((height,width)) #[[0]*height]*width #[[100 for i in range(height)] for j in range(width)]

### Starting position. (y=0,x=0) is top left. 
seedy = 0
seedx = 50
seed = (seedy,seedx) #Icecube[seedy][seedx]


### Create matrix for neighbors. Top left is (y=-1,x=-1) and bottom right is (y=1,x=1). Trying to use (tuples) instead of [lists].
'''
# Flatten for ease of sampling, though initially left like this because I think it's cool.
neighborhood = [ [[-1,-1], [0,-1], [1,-1]], [[-1,0], [0,0], [1,0]], [[-1,1], [0,1], [1,1]] ]
neighborhood = [i for sublist in neighborhood for i in sublist]
# >>> y=1; x=1; neighborhood[y+1][x+1]
# [1, 1]
# >>> y=0; x=0; neighborhood[y+1][x+1]
# [0, 0]
# >>> y=-1; x=-1; neighborhood[y+1][x+1]
# [-1, -1]
'''
#neighborhood = [ (-1,-1), (0,-1), (1,-1), (-1,0), (1,0), (-1,1), (0,1), (1,1) ] # No (0,0)
neighborhood = [ (-1,-1), (0,-1), (1,-1), (-1,0), (0,0), (1,0), (-1,1), (0,1), (1,1) ] # Yes (0,0)







### How to get k for k neighors. Random number between mink and maxk.
def Getk():
  mink = 0
  maxk = 8
  k = random.randint(mink, maxk)  
  return k


### Sample k neighbors. For each neighbor, add relative position to home position to get real coords. Return real position.
def GetNeighbors(home,k):
  # home = (y,x)
  # k = integer
  neighbors = random.sample(neighborhood, k)
  Neighbors = []
  for neighbor in neighbors:
    nposition = tuple(map(sum,zip(home,neighbor)))
    # Check if any y,x is less than 0. No ice is at less than zero. Pass if so; only lose 0-k neighbors to melt. Append otherwise.
    if -1 in nposition: pass
    else: Neighbors.append(nposition)
 
    ##################################################################################################################################
    # This is where the recursion also is. But doesn't melt (no good plot) because it complains before getting to Melt().
    #k = Getk()
    #Neighbors = GetNeighbors(neighbor, k)

  print 'Neighbors:', Neighbors
  return Neighbors




### Def melt function at position. Melt k neighbors. If no neighbors, pass. Call itself again at new neighbors.
# Melt == increase index by one.
def Melt(home, neighbors):
  # home = (y,x)
  # neighbors = [(y1, x1), (y2, x2), ...]
  for neighbor in neighbors:
    if len(neighbors) == 0: pass
    y = neighbor[0]
    x = neighbor[1]
    Icecube[y][x] = Icecube[y][x] + 1
    
    ##################################################################################################################################
    # This is the recursion. Only uncomment these for "best" results.
    k = Getk()
    Neighbors = GetNeighbors(neighbor, k)
    Melt(home, Neighbors)

def main(seed):
  k = Getk()
  Neighbors = GetNeighbors(seed, k)
  Melt(seed, Neighbors)


######################################################################################################################################
# "RuntimeError: maximum recursion depth exceeded while calling a Python object"
# NoteL sometimes it just picks "up", fills nothing, and bails.
# Though can still manually do the plotting after error thrown to see what it did melt. 
# Commented out recursion lines. Try new approach.
main(seed)

### Create canvas with ratio that scales with width,height
c = ROOT.TCanvas('c', 'c', 720*width/height, 720)
#c.SetTopMargin(0)
#c.SetBottomMargin(0)
#c.SetLeftMargin(0)
#c.SetRightMargin(0)

### Generate 2D ice cube.
### Initialize 2D hist to be filled with icecube
hc = ROOT.TH2D('hc', 'hc', width,0,width, height,0,height)
hc.SetStats(0)
hc.SetTitle(';;')

for w in range(width):
  for h in range(height):
    hc.SetBinContent(w,-h+height, Icecube[h][w]) #w,h,Icecube[h][w])

hc.Draw('colz')
c.SaveAs('Drip3_test3.png')
