import ROOT
import numpy as np
import random
import math


#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
ROOT.gStyle.SetPalette(51); # 51:blue. 52:lo=k/hi=w. 53:lo=r/hi=y, 56 rev. 54:lo=b/hi=y. 55:lo=b/hi=r, pale rainbow. none:rainbow 
# Palettes, see https://root.cern.ch/doc/master/classTColor.html#C05. Or for ROOT 5.34: https://people.nscl.msu.edu/~noji/colormap
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(1, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(1);  #0,1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();




# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()
def sqrt(x):
  return math.sqrt(x)
# Redefine pi to something shorter
pi = math.pi




####################################################################################################################################### See #3 for recursive method. It excees recursion depth, so I am trying a new method. More like dripping water, less like fractals.
# Removed recursive and other various bits. Left main functions, for now.

### Try this:
# 1. Drop first seed
# 2. get neighbors
# 3. melt once
# 4. then randomly, though with weights, pick best neighbor, then do 2-4.

### Just try to make a weighted random function that wants to pick things towards the ground. Maybe more macro, not single indices?
### Try to make a "cone" away from the heat, first. From seed pos.




### Dimensions of ice cube
N = 50
height = N
width = N
center = width/2

### Make 100x100 array filled with 100
# Icecube[y][x]
Icecube = np.zeros((height,width))

### Starting position. (y=0,x=0) is top left. 
seedy = 0
seedx = center
seed = (seedy,seedx)


### Create matrix for neighbors. Top left is (y=-1,x=-1) and bottom right is (y=1,x=1). Trying to use (tuples) instead of [lists].
neighborhood = [ (-1,-1), (0,-1), (1,-1), (-1,0), (1,0), (-1,1), (0,1), (1,1) ] # No (0,0)
#neighborhood = [ (-1,-1), (0,-1), (1,-1), (-1,0), (0,0),(1,0), (-1,1), (0,1), (1,1) ] # Yes (0,0)








### How to get k for k neighors. Random number between mink and maxk.
def Getk():
  mink = 0
  maxk = 8
  k = random.randint(mink, maxk)  
  return k


### Sample k neighbors. For each neighbor, add relative position to home position to get real coords. Return real position.
def GetNeighbors(home,k):
  # home = (y,x)
  # k = integer
  neighbors = random.sample(neighborhood, k)
  Neighbors = []
  for neighbor in neighbors:
    nposition = tuple(map(sum,zip(home,neighbor)))

    # Check if any y,x is less than 0. No ice is at less than zero. Pass if so; only lose 0-k neighbors to melt. Append otherwise.
    #if -1 or N in nposition: pass
    if -1 in nposition: pass
    elif N in nposition: pass
    else: Neighbors.append(nposition)
 
    ##################################################################################################################################
    # This is where the recursion also is. But doesn't melt (no good plot) because it complains before getting to Melt().
    #k = Getk()
    #Neighbors = GetNeighbors(neighbor, k)

  print 'k Neighbors:', Neighbors
  return Neighbors




### Sample all neighbors. For each neighbor, add relative position to home position to get real coords. Return real position.
def GetAllNeighbors(home):
  # home = (y,x)
  # k = integer
  Neighbors = []
  for neighbor in neighborhood:
    nposition = tuple(map(sum,zip(home,neighbor)))

    # Check if any y,x is less than 0. No ice is at less than zero. Pass if so; only lose 0-k neighbors to melt. Append otherwise.
    #if -1 or N in nposition: pass
    if -1 in nposition: pass
    elif N in nposition: pass
    else: Neighbors.append(nposition)

  print 'All Neighbors:', Neighbors
  return Neighbors









### Def melt function at position. Melt k neighbors. If no neighbors, pass. Call itself again at new neighbors.
# Melt == increase index by one.
def Melt(home, neighbors):
  # home = (y,x)
  # neighbors = [(y1, x1), (y2, x2), ...]
  for neighbor in neighbors:
    if len(neighbors) == 0: break #pass
    y = neighbor[0]
    x = neighbor[1]
    Icecube[y][x] = Icecube[y][x] + 1
    
    ##################################################################################################################################
    # This is the recursion.
    #k = Getk()
    #Neighbors = GetNeighbors(neighbor, k)
    #Melt(home, Neighbors)


  print 'Melted thus far:', GetMelted()



def GetMelted():
  Melted = []
  for h in range(height):
    meltedx = [i for i, x in enumerate(Icecube[h]) if x > 0.]
    for x in meltedx:
      meltedyx = (h,x)
      Melted.append(meltedyx)

  return Melted






def main(seed):
  k = Getk()
  #Neighbors = GetNeighbors(seed,k)
  Neighbors = GetAllNeighbors(seed)
  Melt(seed, Neighbors)



#main(seed)

#Melted = []
pd = 30/100.

for h in range(int(round(height*pd))):
  print ''
  print 'Depth:', h
  seed = (seedy+h, seedx)
  main(seed)
'''
  meltedx = [i for i, x in enumerate(Icecube[h]) if x > 0.]
  for x in meltedx:
    meltedyx = (h,x)
    Melted.append(meltedyx)
print Melted
'''

'''
for h in range(height):
  seed = (seedy+h, seedx)
  main(seed)
'''




### Create canvas with ratio that scales with width,height
c = ROOT.TCanvas('c', 'c', 720*width/height, 720)

### Initialize 2D hist to be filled with icecube
hc = ROOT.TH2D('hc', 'hc', width,0,width, height,0,height)
hc.SetStats(0)
hc.SetTitle(';;')

for w in range(width):
  for h in range(height):
    hc.SetBinContent(w,-h+height,Icecube[h][w])

hc.Draw('colz')
c.SaveAs('Drip4_test.png')
