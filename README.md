### Hot coffee dripping on ice

I was brewing hot coffee over a large ice cube one day, and as I watched the ice melt, I thought "I wonder if I could simulate that".
And I tried. But it turns out I lost sight of that original scope of a drops and melt streams 
and instead created a recursive random walk with some external force ("gravity", here). This project could also be rethemed to wind pushing a spreading fire, I guess.

There are three generations of this project and I'll go through each attempt briefly at most. 
As always, please see the code for more specific documentation (if it exists...).
This is old code and working through it again is not a priority. 
However, looking through it right now makes me confident that I could actually succeed (spoilers: I abandonded this idea) 
if I just gave it some better thought.

<!--Anyway, I started with a 100x100 grid of zeros. I had the first "drop of hot water" land in the center of the ice cube. 
When a drop touched an index, it "melted" it, or change it from 0 to 1. -->

Below are histograms showing how "hot water" "melted" through a 100x100 "ice cube". All seed points were at the top-center.

The first generation was basic. It melts some amount of random neighbors (0-8), and moves down one row, repeats. 
If all neighbors to melt are outside the cube, break.

![2](Drip2_test.png)

The second generation was the most interesting. It was where I moved to the use of OOP. 
With this, however, I also employed recursion (unknowingly). 
```
RuntimeError: maximum recursion depth exceeded while calling a Python object
```
The idea was that when neighbors were melted,
_those_ neighbors could then melt _more_ neighbors. Because of my method, this turned into a random walk algorithm that 
never had a chance to compute any melted neighbors besides the first of each set.
I could still create plots post-error, though, and here are a few:

![31](Drip3_test1.png)
![32](Drip3_test2.png)
![33](Drip3_test3.png)
![34](Drip3_test4.png)


The third generation removed the recursive elements but also somehow removed all soul from the algorithm. 
I ended up creating some sort of borehole drilling machine for [IceCube](http://icecube.wisc.edu/):

![4](Drip4_test.png)

This was where I eventually abandonded the project for something else.
